import axios from "axios";

const instance = axios.create({
  // To be changed to "/api/" for heroku
  baseURL: "http://localhost:5000/api/"
});

export default instance;
